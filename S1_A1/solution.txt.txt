Make a new file called solution.txt inside s01-a1 folder and answer the following questions based on the image:

1. List the books Authored by Marjorie Green.
Answer:
title_id: BU1032
title: The Busy Executive's Database Guide

title_id: BU2075
title: You Can Combat Computer Stress!
----------------------------------------------
2. List the books Authored by Michael O'Leary.
Answer:
title_id: BU1111
title: Cooking with Computers

title_id: TC7777
title: Unknown / not on the list
----------------------------------------------
3. Write the author/s of "The Busy Executive's Database Guide".
Answer:
au_Iname: Marjorie Green
au_Iname: Abraham Bennet
----------------------------------------------
4. Identify the publisher of "But Is It User Friendly?".
Answer:
pub_id: 1389
pub_name: Algodata Infosystems
----------------------------------------------
5. List the books published by Algodata Infosystems.
Answer:
title: 
The Busy Executive's Database Guide
Cooking with Computers
Straight Talk About Computers
But Is It User Friendly?
Secrets of Silicon Valley
Net Etiquette

