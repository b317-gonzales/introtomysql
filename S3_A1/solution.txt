
• Get all the post with an Author ID of 1.
Answer:
SELECT * FROM posts WHERE author_id = 1;


• Get all the user's email and datetime of creation.
Answer:
SELECT email, datetime_created FROM users;


• Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID.
Answer:
UPDATE posts SET content = 'Hello to the people of the Earth!' WHERE id = 2;


• Delete the user with an email of "johndoe@gmail.com".
Answer:
DELETE FROM users WHERE email = 'johndoe@gmail.com';